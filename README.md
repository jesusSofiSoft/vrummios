**Paquetes necesarios antes de instalación**

npm install -g cordova
npm install -g ios-sim
npm install -g ios-deploy --unsafe-perm=true

**Crear aplicación**

cordova create VrummW com.fuxkar.vrummview "VrummW"

**Agregar plataforma**

cordova platform add ios

**Compilar aplicación**

cordova build ios

**Plugin camara**

cordova plugin add cordova-plugin-ios-camera-permissions --save

cordova plugin add cordova-plugin-camera

**Plugin statusbar** 

cordova plugin add cordova-plugin-statusbar

**Correr proyecto**

cordova emulate iOS

**Archivo config.xml**

    <platform name="ios">
        <allow-intent href="itms:*" />
        <allow-intent href="itms-apps:*" />

        <edit-config target="NSCameraUsageDescription" file="*-Info.plist" mode="merge">
            <string>need camera access to take pictures</string>
        </edit-config>

        <edit-config target="NSPhotoLibraryUsageDescription" file="*-Info.plist" mode="merge">
            <string>need photo library access to get pictures from there</string>
        </edit-config>

        <edit-config target="NSLocationWhenInUseUsageDescription" file="*-Info.plist" mode="merge">
            <string>need location access to find things nearby</string>
        </edit-config>

        <edit-config target="NSPhotoLibraryAddUsageDescription" file="*-Info.plist" mode="merge">
            <string>need photo library access to save pictures there</string>
        </edit-config>

        <!-- images are determined by width and height. The following are supported -->
        <!--iPhone Xs Max (Portrait, Landscape)>
        <splash src="res/screen/ios/iOS_splash_xs.png" width="1242" height="2688"/>
        <splash src="res/screen/ios/iOS_splash_xs2688x1242.png" width="2688" height="1242"/-->
        <!--iPhone XS, X (Portrait, Landscape)>
        <splash src="res/screen/ios/iOS_splash_x.png" width="1125" height="2436"/>
        <splash src="res/screen/ios/iOS_splash_x2436x1125.png" width="2436" height="1125"/-->
        <!--iPhone XR (Portrait, Landscape)>
        <splash src="res/screen/ios/iOS_splash_xr.png" width="828" height="1792"/>
        <splash src="res/screen/ios/iOS_splash_xr1792x828.png" width="1792" height="828"/-->
        <!--iPhone 8 Plus, 7 Plus, 6s Plus (Portrait, Landscape)>
        <splash src="res/screen/ios/iOS_splash_plus.png" width="1242" height="2208"/>
        <splash src="res/screen/ios/iOS_splash_2208x1042.png" width="2208" height="1242"/-->
        <!--iPhone 8, 7, 6s (Portrait, Landscape)>
        <splash src="res/screen/ios/iOS_splash_6_8.png" width="750" height="1334"/>
        <splash src="res/screen/ios/iOS_splash_6_81334x750.png" width="1334" height="750"/-->
        <!--iPhone SE (Portrait, Landscape)>
        <splash src="res/screen/ios/iOS_splash_5.png" width="640" height="1136"/>
        <splash src="res/screen/ios/iOS_splash_51136x640.png" width="1136" height="640"/-->
        <!--9.7" iPad, 7.9" iPad mini 4 (Portrait, Landscape)>
        <splash src="res/screen/ios/iOS_splash_1536x2048.png" width="1536" height="2048"/>
        <splash src="res/screen/ios/iOS_splash_2048X1536.png" width="2048" height="1536"/-->
        <!-- >
        <splash src="res/screen/ios/iOS_splash_320x480.png" width="320" height="480"/>
        <splash src="res/screen/ios/iOS_splash_640x960.png" width="640" height="960"/>
        <splash src="res/screen/ios/iOS_splash_768x1024.png" width="768" height="1024"/>
        <splash src="res/screen/ios/iOS_splash_1024X768.png" width="1024" height="768"/-->

        
        <!-- iOS 8.0+ -->
        <!-- iPhone 6 Plus  -->
        <icon src="res/icon/ios/iOS_Icon180.png" width="180" height="180" />
        <!-- iOS 7.0+ -->
        <!-- iPhone / iPod Touch  -->
        <icon src="res/icon/ios/Icon60.png" width="60" height="60" />
        <icon src="res/icon/ios/Icon120.png" width="120" height="120" />
        <!-- iPad -->
        <icon src="res/icon/ios/Icon76.png" width="76" height="76" />
        <icon src="res/icon/ios/Icon152.png" width="152" height="152" />
        <!-- iOS 6.1 -->
        <!-- Spotlight Icon -->
        <icon src="res/icon/ios/Icon40.png" width="40" height="40" />
        <icon src="res/icon/ios/Icon80.png" width="80" height="80" />
        <!-- iPhone / iPod Touch -->
        <icon src="res/icon/ios/Icon57.png" width="57" height="57" />
        <icon src="res/icon/ios/Icon114.png" width="114" height="114" />
        <!-- iPad -->
        <icon src="res/icon/ios/Icon72.png" width="72" height="72" />
        <icon src="res/icon/ios/Icon144.png" width="144" height="144" />
        <!-- iPhone Spotlight and Settings Icon -->
        <icon src="res/icon/ios/Icon29.png" width="29" height="29" />
        <icon src="res/icon/ios/Icon58.png" width="58" height="58" />
        <!-- iPad Spotlight and Settings Icon -->
        <icon src="res/icon/ios/Icon50.png" width="50" height="50" />
        <icon src="res/icon/ios/Icon100.png" width="100" height="100" />

    </platform>

    <preference name="StatusBarOverlaysWebView" value="false" />

    <preference name="StatusBarBackgroundColor" value="#1D1D1D" />

    <preference name="StatusBarDefaultScrollToTop" value="true" />

    <preference name="StatusBarStyle" value="blackopaque" />